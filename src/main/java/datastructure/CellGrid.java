package datastructure;

import cellular.CellState;

import java.util.ArrayList;

public class CellGrid implements IGrid {
    //Feltvariabler:
    int cols;
    int rows;

    CellState[][] grid;

    public CellGrid(int rows, int columns, CellState initialState) {
		this.rows = rows;
        this.cols = columns;
        this.grid = new CellState[rows][columns];
	}


    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        if (row < 0 || row > rows || column < 0 || column > cols) {
            throw new IndexOutOfBoundsException("Index er ikke med.");
        }
        grid[row][column] = element;
        
    }

    @Override
    public CellState get(int row, int column) {
        if (row < 0 || row > rows || column < 0 || column > cols) {
            throw new IndexOutOfBoundsException("Index er ikke med.");
        }
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid newGrid = new CellGrid(this.rows, this.cols, CellState.DEAD);
        for (int row=0; row < rows; row++){
            for (int column = 0; column<cols; column++){
                newGrid.set(row, column, this.get(row, column));
            }
        }
        return newGrid;
    }
    
}
